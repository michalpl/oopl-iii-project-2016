Michał Platta

This program is a simple example of students management. We can view list of students by album number or groups and add new student. We can find full information of student and them marks and add new.

## How to compile ##

I've prepared packaged version of this app already. If you want to compile project on your own you can use:

* IntelliJ IDEA IDE to compile project or,
* Maven to compile and pack an app to jar archive.

## How to run this app ##

     java -cp SimpleStudentsMenager-1.0.0-SNAPSHOT.jar pl.edu.pg.ftims.App