package pl.edu.pg.ftims.date;

/**
 * 
 * @author Michal Platta
 *
 */
public class Date {
	private int day;
	private int month;
	private int year;
	
	public int getDay() {
		return day;
	}
	
	/**
	 * Set day only impossible day
	 * 
	 * @param day							day of moth in int
	 * @throws ImpossibleDateException
	 */
	public void setDay(int day) throws ImpossibleDateException {
		if ((day > 31) || (day < 1)) {
			throw new ImpossibleDateException();
		} else this.day = day;
	}
	
	public int getMonth() {
		return month;
	}

	/**
	 * Set moth only impossible month
	 * 
	 * @param month							month of year in int
	 * @throws ImpossibleDateException
	 */
	public void setMonth(int month) throws ImpossibleDateException {
		if ((month > 12) || (month < 1)) {
			throw new ImpossibleDateException();
		} else this.month = month;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	@Override
	public String toString() {
		return day + "/" + month + "/" + year; 
	}
	
	public Date(int day, int month, int year) {
		try {
			this.setDay(day);
			this.setMonth(month);
		} catch (ImpossibleDateException e) {
			e.printStackTrace();
		}
		
		this.year = year;
	}
	
	public Date() {}
}
