package pl.edu.pg.ftims.students;

import java.util.Comparator;

import pl.edu.pg.ftims.students.Students;

/**
 * 
 * @author Michal Platta
 *
 */
public class StudentsComparator implements Comparator<Students>{

	@Override
	public int compare(Students arg0, Students arg1) {
		if (arg0.getAlbumNo() > arg1.getAlbumNo()) return 1;
		else return -1;
	}

}