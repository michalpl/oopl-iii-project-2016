package pl.edu.pg.ftims.display;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import pl.edu.pg.ftims.catcher.Catcher;
import pl.edu.pg.ftims.students.*;
import pl.edu.pg.ftims.date.Date;
import pl.edu.pg.ftims.display.Displayable;
import pl.edu.pg.ftims.menuDisplayer.MenuDisplayer;
import pl.edu.pg.ftims.subject.Subject;

/**
 * 
 * @author Michal Platta
 *
 *	Main Menu 
 */
public class MainDisplay implements Displayable, Menuable {
	@Override
	public void display() {
		Set<Students> studentsList = new TreeSet<Students>(new StudentsComparator());
		
		Subject oop = new Subject("Object Oriented Programming", "OOP", new HashMap<Integer, LinkedList<Integer>>());
		Subject capp = new Subject("Concurrent and Parallel Programming", "CAPP", new HashMap<Integer, LinkedList<Integer>>());
		Subject math = new Subject("Mathematica", "MATH", new HashMap<Integer, LinkedList<Integer>>());
		
		LinkedList<Subject> subjectsList = new LinkedList<Subject>();
		
		subjectsList.add(oop);
		subjectsList.add(capp);
		subjectsList.add(math);
		
		Students.setFirstAlbumNuber(155836);
		
		studentsList.add(new Students("Michal", "XYZ", 1, "FTIMS", new Date(11, 11, 1921)));
		studentsList.add(new Students("Adam", "ZXC", 1, "FTIMS", new Date(12, 11, 1921)));
		studentsList.add(new Students("Seba", "ABC", 3, "FTIMS", new Date(13, 11, 1921)));
		studentsList.add(new Students("Robert", "QWE", 3, "FTIMS", new Date(14, 11, 1921)));
		
		Random generator = new Random();
		
		for (Students student : studentsList) {
			oop.getStudentsMarks().put(student.getAlbumNo(), new LinkedList<Integer>());
			capp.getStudentsMarks().put(student.getAlbumNo(), new LinkedList<Integer>());
			math.getStudentsMarks().put(student.getAlbumNo(), new LinkedList<Integer>());
			for (int i = 0; i < generator.nextInt(7) + 1; i++) {
				oop.getStudentsMarks().get(student.getAlbumNo()).add(generator.nextInt(3) + 3);
				capp.getStudentsMarks().get(student.getAlbumNo()).add(generator.nextInt(3) + 3);
				math.getStudentsMarks().get(student.getAlbumNo()).add(generator.nextInt(3) + 3);
			}
		}
		
		int chose = 0;	
		Boolean done = false;
		
		do {
			chose = Catcher.catchInt(new MenuDisplayer(menu()));
			
			switch (chose) {
				case 1: Students.showSetOfStudents(studentsList, 0, studentsList.size());
					break;
				case 2: StudentsMenager.findStudents(studentsList, subjectsList);
					break;
				case 3: Students.showSetOfStudentsByGroup(studentsList);
					break;
				case 4: studentsList.add(StudentsMenager.studentCreator());
					break;
				case 5: done = true;
			}
		} while (!done);
		
		System.out.println("----------------------------------------------------------------");
		
		Catcher.closeScanner();
	}

	@Override
	public String menu() {
		return "1 - Students list\n2 - Find student marks\n"
		+ "3 - Students group list\n4 - Add student\n5 - Exit";
	}
}
