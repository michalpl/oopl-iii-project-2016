package pl.edu.pg.ftims.date;

/**
 * 
 * @author Michal Platta
 *
 */
public class ImpossibleDateException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Impossible Date\n";
	}
	
	@Override
	public String toString() {
		return "Impossible Date\n";
	}
}
