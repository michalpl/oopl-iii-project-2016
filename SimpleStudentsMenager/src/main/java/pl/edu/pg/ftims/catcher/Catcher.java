package pl.edu.pg.ftims.catcher;

import java.util.Scanner;

import pl.edu.pg.ftims.display.Displayable;

/**
 * 
 * @author Michal Platta
 *
 */
public class Catcher {
	
	private static Scanner reader;
	
	/**
	 * Display menu and wait for user write any Integer
	 * 
	 * @param menu		class that implements Displayable for display menu
	 * @return 			give caught Integer
	 */
	public static final Integer catchInt(Displayable menu) {
		Integer result = null;
		do 
		{
			System.out.println("----------------------------------------------------------------");
			menu.display();
			reader = new Scanner(System.in);
			
			try {
				result = reader.nextInt();
			} catch (Exception e) {
				System.out.println("This char is not integer number");
				result = null;
			}
		} while (result == null);
		
		return result;
	}
	
	/**
	 * Display menu and wait for user write any String
	 * 
	 * @param menu		class that implements Displayable for display menu
	 * @return 			give caught String
	 */
	public static final String catchStr(Displayable menu) {
		String result = null;
		do 
		{
			System.out.println("----------------------------------------------------------------");
			menu.display();
			reader = new Scanner(System.in);
			
			try {
				result = reader.nextLine();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				result = null;
			}
		} while (result == null);
		
		return result;
	}
	
	/**
	 * Close all open IO scanner
	 */
	public static final void closeScanner() {
		reader.close();
	}
}
