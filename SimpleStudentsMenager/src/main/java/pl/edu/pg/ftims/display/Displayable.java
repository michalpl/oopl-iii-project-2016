package pl.edu.pg.ftims.display;

/**
 * 
 * @author Michal Platta
 *
 */
public interface Displayable {
	public void display();
}
