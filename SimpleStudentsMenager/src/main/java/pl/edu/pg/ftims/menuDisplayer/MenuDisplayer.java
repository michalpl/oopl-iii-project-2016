package pl.edu.pg.ftims.menuDisplayer;

import pl.edu.pg.ftims.display.Displayable;

/**
 * 
 * @author Michal Platta
 *
 */
public class MenuDisplayer implements Displayable {
	private String text;
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public void display() {
		System.out.println(text);
	}
	
	public MenuDisplayer(String text) {
		this.text = text;
	}

}
