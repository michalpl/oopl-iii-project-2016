package pl.edu.pg.ftims.faculty;

/**
 * 
 * @author Michal Platta
 *
 */
public abstract class Faculty {
	protected String faculty;

	public String getFaculty() {
		return this.faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
}
