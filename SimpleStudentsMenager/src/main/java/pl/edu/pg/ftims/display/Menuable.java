package pl.edu.pg.ftims.display;

/**
 * 
 * @author Michal Platta
 *
 */
public interface Menuable {
	public String menu();

}
