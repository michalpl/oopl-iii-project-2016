package pl.edu.pg.ftims.subject;

import java.util.LinkedList;
import java.util.Map;

/**
 * 
 * @author Michal Platta
 *
 */
public class Subject {
	private String name;
	private String shortName;
	private Map<Integer, LinkedList<Integer>> studentsMarks;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Map<Integer, LinkedList<Integer>> getStudentsMarks() {
		return studentsMarks;
	}

	public void setStudentsMarks(Map<Integer, LinkedList<Integer>> studentsMarks) {
		this.studentsMarks = studentsMarks;
	}
	
	public Subject(String name, String shortName, Map<Integer, LinkedList<Integer>> studentsMarks) {
		this.name = name;
		this.shortName = shortName;
		this.studentsMarks = studentsMarks;

	}
}
