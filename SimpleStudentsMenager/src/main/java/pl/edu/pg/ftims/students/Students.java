package pl.edu.pg.ftims.students;

import pl.edu.pg.ftims.group.Group;
import pl.edu.pg.ftims.students.StudentsComparator;
import pl.edu.pg.ftims.date.Date;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * @author Michal Platta
 *
 */
public class Students extends Group {
	private Integer albumNo;
	private static Integer nextAlbumNo;
	private String name;
	private String surname;
	public Date birthday;
	
	public static void setFirstAlbumNuber(Integer number) {
		Students.nextAlbumNo = number; 
	}
	
	public Integer getAlbumNo() {
		return albumNo;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	
	@Override
	public String toString() {
		return albumNo + "\t\t" + name + "\t\t  " + surname + "\t\t\t" + birthday.getDay() + "." + birthday.getMonth() + "." + birthday.getYear();
	}
	
	/**
	 * Print Students Set
	 * 
	 * @param setOfStudents		Students Set to printing
	 * @param begin				start iteration place
	 * @param end				end iteration place
	 */
	public static void showSetOfStudents(Set<Students> setOfStudents, int begin, int end) {
		if (end < begin) return;
		
		System.out.println("----------------------------------------------------------------");
		System.out.println("Album number:\tName:\t\tSurname:\t\tBirthday:");
		
		int iterator = 0;
		for (Students student : setOfStudents) {
			if (iterator > end) return;
			if (iterator >= begin) {	
				System.out.println(student);
			}
			
			iterator++;
		}
		System.out.println("----------------------------------------------------------------");
	}
	
	/**
	 * Print Students Set divided by Group
	 *  
	 * @param studentsList		Students Set to printing
	 */
	public static void showSetOfStudentsByGroup(Set<Students> studentsList) {
		Map<Integer, TreeSet<Students>> studentByGorup = new HashMap<Integer, TreeSet<Students>>();
		
		System.out.println("----------------------------------------------------------------");
		for (Students student : studentsList) {
			if (studentByGorup.get(student.getGroup()) != null) {
				studentByGorup.get(student.getGroup()).add(student);
			} else {
				studentByGorup.put(student.getGroup(), new TreeSet<Students>(new StudentsComparator()));
				studentByGorup.get(student.getGroup()).add(student);
			}
		}
		
		for (Map.Entry<Integer, TreeSet<Students>> group : studentByGorup.entrySet()) {
			System.out.println("Group: " + group.getKey());
			for (Students student : group.getValue()) {
				System.out.println("\t" + student);
			}
		}
		System.out.println("----------------------------------------------------------------");
	}
	
	public Students(String name, String surname, Integer group, String faculty, Date birthday) {
		this.albumNo = nextAlbumNo;
		this.name = name;
		this.surname = surname;
		this.group = group;
		this.birthday = birthday;
		this.faculty = faculty;
		
		Students.nextAlbumNo++;
	}
}
