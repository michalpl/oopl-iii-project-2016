package pl.edu.pg.ftims.students;

import java.util.List;
import java.util.Set;

import pl.edu.pg.ftims.catcher.Catcher;
import pl.edu.pg.ftims.date.Date;
import pl.edu.pg.ftims.date.ImpossibleDateException;
import pl.edu.pg.ftims.menuDisplayer.MenuDisplayer;
import pl.edu.pg.ftims.students.Students;
import pl.edu.pg.ftims.subject.Subject;

/**
 * 
 * @author Michal Platta
 *
 * A set of methods that operate on the class Students and Subject 
 */
public class StudentsMenager {
	
	/**
	 * Display menu and write Student parameters step by step 
	 * 
	 * @return					new Student with the user supplied parameters
	 */
	public static Students studentCreator() {
		String studentName;
		String studentSurname;
		Integer studentGroup;
		String studentFaculty;
		Date studentBirthday = new Date();
		Boolean dateDone = false;
		
		System.out.println("----------------------------------------------------------------");
		
		studentName = Catcher.catchStr(new MenuDisplayer("Student name: "));
		studentSurname = Catcher.catchStr(new MenuDisplayer("Student surname: "));
		
		do {
			try {
				studentBirthday.setDay(Catcher.catchInt(new MenuDisplayer("Student day of birth: ")));
				studentBirthday.setMonth(Catcher.catchInt(new MenuDisplayer("Student month of birth: ")));
				dateDone = true;
			} catch (ImpossibleDateException e) {
				System.out.println(e.getMessage());
				dateDone = false;
			} 
		} while (!dateDone); 
		
		studentBirthday.setYear(Catcher.catchInt(new MenuDisplayer("Student year of birth: ")));
		studentFaculty = Catcher.catchStr(new MenuDisplayer("Student faculty: "));
		studentGroup = Catcher.catchInt(new MenuDisplayer("Student group: "));
		
		System.out.println("----------------------------------------------------------------");
		
		return (new Students(studentName, studentSurname, studentGroup, studentFaculty, studentBirthday));
	}
	
	/**
	 * Find Student by AlbumNo and this marks by Subject
	 * 
	 * @param studentsList			Set of Students in which we seek Students parameters by AlbumNo
	 * @param subjectsList			List of Subject in which we seek Marks by Subjects
	 */
	static public void findStudents(Set<Students> studentsList, List<Subject> subjectsList) {
		Integer albumNo = null;
		Integer chose = null;
		Boolean done = false;
		Boolean studentExist = null;
		String text;
		
		System.out.println("----------------------------------------------------------------");
		albumNo = Catcher.catchInt(new MenuDisplayer("Write student album number: "));
		if (albumNo == -1) return;
		
		do {		
			do {
				text = "";
				if (studentExist != null) if (studentExist == false) {
					System.out.println("----------------------------------------------------------------");
					albumNo = Catcher.catchInt(new MenuDisplayer("This student does not exist!\n"
							+ "Write -1 to back or write again student album number: "));
					if (albumNo == -1) return;
				}
				for (Students student : studentsList) {
					if (student.getAlbumNo().equals(albumNo)) {
						text += "\nStudent: " + student.getName() + " " + student.getSurname();
						text += "\nBirthday: " + student.birthday;
						text += "\nFaculty: " + student.getFaculty() + " Group: " + student.getGroup(); 
						text += "\n";
						
						for (Subject subject : subjectsList) {
							text += "\n" + subject.getName() + " [" + subject.getShortName()
								+ "]" + ": \t" + subject.getStudentsMarks().get(albumNo);
						}
						
						studentExist = true;
						break;
					} else studentExist = false;
				}
			} while (!studentExist);
			
			System.out.println("----------------------------------------------------------------");
			System.out.println(text);
			System.out.println("----------------------------------------------------------------");
			
			chose = Catcher.catchInt(new MenuDisplayer("\n1 - Add marks\t 2 - Back"));
		
			switch (chose) {
				case 1: addMarks(albumNo, subjectsList);
					break;
				case 2: done = true;
			}
		} while (!done);
	}
	
	/**
	 * Add marks to Students by AlbumNo
	 * 
	 * @param albumNo				key to find Student
	 * @param subjectList			list of Subject 
	 */
	private static void addMarks(Integer albumNo, List<Subject> subjectList) {
		String subjectShortcut = null;
		Integer mark = null;
		Boolean done = false;
		
		do {
			if (mark == null) {
				System.out.println("----------------------------------------------------------------");
				mark = Catcher.catchInt(new MenuDisplayer("Give: 0 to back or Give the new mark: "));
			} else {
				System.out.println("----------------------------------------------------------------");
				mark = Catcher.catchInt(new MenuDisplayer("Wrong mark! Acceptable marks 2 to 5 only Integer\n"
						+ "Give: 0 to back or Give the new mark: "));
			}
			
			if (mark == 0) return;
		} while ((mark > 5) || (mark < 2));
		
		do {
			if (subjectShortcut == null) {
				System.out.println("----------------------------------------------------------------");
				subjectShortcut = Catcher.catchStr(new MenuDisplayer("Give: 0 to back or "
						+ "Give subject shortcut: "));
			}
			else{
				System.out.println("----------------------------------------------------------------");
				subjectShortcut = Catcher.catchStr(new MenuDisplayer("This shortcut does not exist!\n"
					+ "Give: 0 to back or Give subject shortcut: "));
			}
			
			if (subjectShortcut == "0") return;
			
			for (Subject subject : subjectList) {
				if (subject.getShortName().equals(subjectShortcut)) {
					subject.getStudentsMarks().get(albumNo).add(mark);
					done = true;
					break;
				}
				else subjectShortcut = null;
			}
		} while (!done);
	}
}
