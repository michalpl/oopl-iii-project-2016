package pl.edu.pg.ftims;

import pl.edu.pg.ftims.display.MainDisplay;

/**
 * 
 * @author Michal Platta
 *
 */
public class App {

	public static void main(String[] args) {
		System.out.println("Simple student menager, Author: Michal Platta\n");
		MainDisplay mainMenu = new MainDisplay();
		mainMenu.display();
	}
}
