package pl.edu.pg.ftims.group;

import pl.edu.pg.ftims.faculty.*;

/**
 * 
 * @author Michal Platta
 *
 */
public abstract class Group extends Faculty {
	protected Integer group;

	public Integer getGroup() {
		return this.group;
	}
	public void setGroup(Integer group) {
		this.group = group;
	}
}
